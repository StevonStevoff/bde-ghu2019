from flask import Flask, Blueprint
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object("config")
db = SQLAlchemy(app)
migrate = Migrate(app, db)

api = Api(app)

from api import models
from api.resources import Account, Route, Places, Groups

# Route
api.add_resource(Account.Register, '/account/register')
api.add_resource(Account.Login, '/account/login')
api.add_resource(Route.Route, '/route/route')
api.add_resource(Route.RouteNamed, '/route/route/<int:id>')
api.add_resource(Route.RoutePoints, '/route/points')
api.add_resource(Route.RoutePointsNamed, '/route/points/<int:route_id>')
api.add_resource(Places.Place,'/places/place')
api.add_resource(Places.PlaceSearch,'/places/place/<lat>;<lng>')
api.add_resource(Places.PlaceDel,'/places/place/<id>')
api.add_resource(Places.City, '/places/city')
api.add_resource(Places.CityNamed, '/places/city/<name>')
api.add_resource(Places.Country, '/places/country')
api.add_resource(Places.CountryNamed, '/places/country/<name>')
api.add_resource(Groups.CreateGroup, '/groups/add')
api.add_resource(Groups.GetGroups, '/groups/list')
api.add_resource(Groups.AddFollower, '/groups/follower')
api.add_resource(Groups.LevelUp, '/groups/level')