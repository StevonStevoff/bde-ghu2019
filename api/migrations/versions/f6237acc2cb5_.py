"""empty message

Revision ID: f6237acc2cb5
Revises: 
Create Date: 2019-11-17 06:27:52.133991

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f6237acc2cb5'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('country',
    sa.Column('country', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('country')
    )
    op.create_table('reward',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('cost', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('route',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('reward', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('email', sa.String(), nullable=False),
    sa.Column('surname', sa.String(), nullable=True),
    sa.Column('forename', sa.String(), nullable=True),
    sa.Column('pass_hash', sa.String(), nullable=True),
    sa.Column('points', sa.Integer(), nullable=True),
    sa.Column('distance', sa.Float(), nullable=True),
    sa.Column('postcode', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('email')
    )
    op.create_table('city',
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('country_name', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['country_name'], ['country.country'], ),
    sa.PrimaryKeyConstraint('name')
    )
    op.create_table('redeem',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_email', sa.String(), nullable=False),
    sa.Column('reward_id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['reward_id'], ['reward.id'], ),
    sa.ForeignKeyConstraint(['user_email'], ['user.email'], ),
    sa.PrimaryKeyConstraint('id', 'user_email', 'reward_id')
    )
    op.create_table('groups',
    sa.Column('user_email', sa.String(), nullable=False),
    sa.Column('city_name', sa.String(), nullable=False),
    sa.Column('level', sa.Integer(), nullable=True),
    sa.Column('multiplier', sa.Float(), nullable=True),
    sa.Column('size', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['city_name'], ['city.name'], ),
    sa.ForeignKeyConstraint(['user_email'], ['user.email'], ),
    sa.PrimaryKeyConstraint('user_email', 'city_name')
    )
    op.create_table('place',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('information', sa.String(), nullable=True),
    sa.Column('city_name', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['city_name'], ['city.name'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('route_places',
    sa.Column('route_id', sa.Integer(), nullable=False),
    sa.Column('places_id', sa.Integer(), nullable=False),
    sa.Column('order', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['places_id'], ['place.id'], ),
    sa.ForeignKeyConstraint(['route_id'], ['route.id'], ),
    sa.PrimaryKeyConstraint('route_id', 'places_id')
    )
    op.create_table('visits',
    sa.Column('user_email', sa.String(), nullable=False),
    sa.Column('place_id', sa.Integer(), nullable=False),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['place_id'], ['place.id'], ),
    sa.ForeignKeyConstraint(['user_email'], ['user.email'], ),
    sa.PrimaryKeyConstraint('user_email', 'place_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('visits')
    op.drop_table('route_places')
    op.drop_table('place')
    op.drop_table('groups')
    op.drop_table('redeem')
    op.drop_table('city')
    op.drop_table('user')
    op.drop_table('route')
    op.drop_table('reward')
    op.drop_table('country')
    # ### end Alembic commands ###
