from flask import jsonify, request
from flask_restful import Resource
from api import db, models
from json import dumps

class Register(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['email']
        pwd = json_data['pass_hash']
        sname = json_data['surname']
        fname = json_data['forename']
        home = json_data['postcode']

        users = models.User.query.get(email)
        if users != None:
            return dumps(users.serialize()),201

        account = models.User(email=email,surname=sname,forename=fname,pass_hash=pwd,postcode=home)
        db.session.add(account)
        db.session.commit()

        return dumps(account.serialize())

class Login(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['email']
        pwd = json_data['pass_hash']

        account = models.User.query.get(email)
        if account == None:
            return 201
        
        if account.pass_hash != pwd:
            return 202
        
        return json.dumps(account.serialize())