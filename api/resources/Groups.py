from flask import request
from flask_restful import Resource
from sqlalchemy import and_
from api import db,models
from json import dumps

import googlemaps

gmaps = googlemaps.Client(key='AIzaSyAYuIqfFDtJtbN0aSDwqlnzmd1AJYsM8IU')

class CreateGroup(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['user_email']
        pc = json_data['postcode']
        city = json_data['city_name']

        group = models.Groups.query.filter(and_(models.Groups.user_email==email, models.Groups.city_name==city)).first()

        # create db record if new group
        if group == None:
            dist = gmaps.distance_matrix(pc, city)['rows'][0]['elements'][0]['distance']['value']
            multi = round(1 + (dist/1.6)/100000, 2)

            if models.User.query.get(email) != None and models.City.query.get(city) != None:
                new_group = models.Groups(user_email=email,level=0,multiplyer=multi,size=1)
                new_group.city = models.City.query.get(city)
                user = models.User.query.get(email)
                user.cities.append(new_group)
                db.session.add(new_group)
                db.session.commit()
                
                return dumps(new_group.serialize())
            return 202
        else:
            return dumps(group.serialize()), 201

class AddFollower(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['user_email']
        city = json_data['city_name']
        group = models.Groups.query.filter(and_(models.Groups.user_email==email, models.Groups.city_name==city)).first()
        group.size += 1
        db.session.commit()

        return (group.serialize())

class LevelUp(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['user_email']
        city = json_data['city_name']
        group = models.Groups.query.filter(and_(models.Groups.user_email==email, models.Groups.city_name==city)).first()
        group.level += 1
        db.session.commit()

        return (group.serialize())

class GetGroups(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        email = json_data['user_email']

        groups = models.Groups.query.filter(models.Groups.user_email==email).all()

        return [dumps(g.serialize()) for g in groups]

