from flask import jsonify, request
from flask_restful import Resource
from api import db, models
import json

class PlaceSearch(Resource):
    def get(self, lat, lng):
        return lng

class PlaceDel(Resource):
    def delete(self, id):
        place = models.Place.query.get(id)
        if place == None:
            return 201

        db.session.delete(place)
        db.session.commit()
        return 200

class Place(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        name = json_data['name']
        description = json_data['description']
        information = json_data['information']
        city = json_data['city']

        place = models.Place(name=name, description=description, information=information, city_name=city)
        db.session.add(place)
        db.session.commit()

        return json.dumps(place.serialize())

class CityNamed(Resource):
    def get(self, name):
        if name == "":
            # return all data if no search
            data = models.City.query.all()
            return json.dumps(data.serialize())
        else:
            search = models.City.query.get(name)
            if search == None:
                return 201
            else:
                return json.dumps(search.serialize())

    def delete(self, name):
        city = models.City.query.get(name)
        if city == None:
            return 201

        db.session.delete(city)
        db.session.commit()
        return 200

class City(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        name = json_data['name']
        country = json_data['country']

        city = models.City.query.get(name)
        if city != None:
            return 201

        city = models.City(name=name, country_name=country)
        db.session.add(city)
        db.session.commit()

        return json.dumps(city.serialize())

class CountryNamed(Resource):
    def get(self, name):
        if name == "":
            # return all data if no search
            data = models.Country.query.all()
            return json.dumps(data.serialize())
        else:
            search = models.Country.query.get(name)
            if search == None:
                return 201
            else:
                return json.dumps(search.serialize())

    def delete(self, name):
        country = models.Country.query.get(name)
        if city == None:
            return 201

        db.session.delete(country)
        db.session.commit()
        return 200

class Country(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        name = json_data['country']

        country = models.Country.query.get(name)
        if country != None:
            return 201

        country = models.Country(country=name)
        db.session.add(country)
        db.session.commit()

        return json.dumps(country.serialize())
