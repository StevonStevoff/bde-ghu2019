from flask import jsonify, request
from flask_restful import Resource
from api import db, models
from json import dumps

class RouteNamed(Resource):
    def get(self, id):
        if id == -1:
            # return all data if no search
            data = models.Route.query.all()
            return json.dumps(data.serialize())
        else:
            search = models.Route.query.get(id)
            if search == None:
                return 201
            else:
                return json.dumps(search.serialize())

    def delete(self, id):
        route = models.Route.query.get(id)
        if route == None:
            return 201

        db.session.delete(route)
        db.session.commit()
        return 200

class Route(Resource):
    def post(self):
        json_data = request.get_json(force=True)
        name = json_data['name']
        description = json_data['description']
        reward = json_data['reward']

        route = models.Route(name=name, description=description, reward=reward, distance=distance)
        db.session.add(route)
        db.session.commit()

        return json.dumps(route.serialize())

class RoutePointsNamed(Resource):
    def get(self, route_id):
        # get route
        route = models.Route.query.get(route_id)
        if route == None:
            return 201

        # return path points
        return json.dumps(route.places.serialize())

    def delete(self, route_id):
        def get(self, route_id):
            # get route
            route = models.Route.query.get(route_id)
            if route == None:
                return 201

            # remove places from route
            routePlaces = route.places
            for place in routePlaces:
                db.session.delete(place)
            db.session.commit()
            return 200

class RoutePoints(Resource):
    def post(self):
        # get data
        json_data = request.get_json(force=True)
        route_id = json_data['route']
        place_id = json_data['place']

        # get route and place
        route = models.Route.query.get(route_id)
        place = models.Route.query.get(place_id)
        if route == None or place == None:
            return 201

        # add new place in route points
        rp = models.RoutePlaces(len(route.places))
        rp.place = place
        route.places.append(rp)
        db.session.add(rp)
        db.session.commit()

        return json.dumps(rp.serialize())
