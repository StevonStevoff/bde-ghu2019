from api import db
from sqlalchemy.inspection import inspect
import json

# serializer class to be inherited by models
class ModelSerializer():
    def serialize(self):
        fields = {}
        for field in dir(self):
            if not field.startswith('_') and field != 'metadata':
                try:
                    data = self.__getattribute__(field)
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except:
                    continue

        return fields

# user data
class User(db.Model, ModelSerializer):
    email = db.Column(db.String, primary_key=True)
    surname = db.Column(db.String)
    forename = db.Column(db.String)
    pass_hash = db.Column(db.String)
    points = db.Column(db.Integer)
    distance = db.Column(db.Float)
    postcode = db.Column(db.String)

# place data
class Place(db.Model, ModelSerializer):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    description = db.Column(db.String)
    information = db.Column(db.String)
    city_name = db.Column("city_name", db.String, db.ForeignKey("city.name"))
    city = db.relationship("City", lazy=True)

class City(db.Model, ModelSerializer):
    name = db.Column(db.String, primary_key=True)
    country_name = db.Column("country_name", db.String, db.ForeignKey("country.country"))
    country = db.relationship("Country", lazy=True)

class Country(db.Model, ModelSerializer):
    country = db.Column(db.String, primary_key=True)

# route data
class Route(db.Model, ModelSerializer):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    description = db.Column(db.String)
    reward = db.Column(db.Integer)

# route-place link
class RoutePlaces(db.Model, ModelSerializer):
    route_id = db.Column("route_id", db.Integer, db.ForeignKey("route.id"), primary_key=True)
    place_id = db.Column("places_id", db.Integer, db.ForeignKey("place.id"), primary_key=True)
    order = db.Column(db.Integer)
    place = db.relationship("Place", lazy="subquery", backref=db.backref("routes", lazy=True))
    route = db.relationship("Route", lazy="subquery", backref=db.backref("places", lazy=True))

# visit data and group data for user
# user-visit link
class Visits(db.Model, ModelSerializer):
    user_email = db.Column("user_email", db.String, db.ForeignKey("user.email"), primary_key=True)
    place_id = db.Column("place_id", db.Integer, db.ForeignKey("place.id"), primary_key=True)
    datetime = db.Column("datetime", db.DateTime)
    user = db.relationship("User", lazy="subquery", backref=db.backref("places", lazy=True))
    place = db.relationship("Place", lazy="subquery")

# user-group visit
class Groups(db.Model, ModelSerializer):
    user_email = db.Column("user_email", db.String, db.ForeignKey("user.email"), primary_key=True)
    city_name = db.Column("city_name", db.String, db.ForeignKey("city.name"), primary_key=True)
    level = db.Column(db.Integer)
    multiplier = db.Column(db.Float)
    size = db.Column(db.Integer)
    user = db.relationship("User", lazy="subquery", backref=db.backref("cities", lazy=True))
    city = db.relationship("City", lazy="subquery")

# reward data
class Reward(db.Model, ModelSerializer):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    description = db.Column(db.String)
    cost = db.Column(db.Integer)

class Redeem(db.Model, ModelSerializer):
    id = db.Column("id", db.Integer, primary_key=True)
    user_email = db.Column("user_email", db.String, db.ForeignKey("user.email"), primary_key=True)
    reward_id = db.Column("reward_id", db.Integer, db.ForeignKey("reward.id"), primary_key=True)
    datetime = db.Column("datetime", db.DateTime)
    user = db.relationship("User", lazy="subquery", backref=db.backref("rewards", lazy=True))
    reward = db.relationship("Reward", lazy="subquery", backref=db.backref("users", lazy=True))
