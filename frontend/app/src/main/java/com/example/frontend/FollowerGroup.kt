package com.example.frontend

class Follower(val p:String, val gS: Int, val ml: Double, val gL: Int)
{
    val postcode : String = p
    val groupSize : Int = gS
    val multiplier : Double = ml
    val groupLevel : Int = gL
}