package com.example.frontend

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.view.View


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }
    fun loginActivity(view: View){
        val intent = Intent(this, Login::class.java)
        // start your next activity
        startActivity(intent)
    }
    fun RegistrationActivity(view: View){
        val intent = Intent(this, Registration::class.java)
        // start your next activity
        startActivity(intent)
    }

}
