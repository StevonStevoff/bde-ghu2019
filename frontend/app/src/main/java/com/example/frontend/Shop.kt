package com.example.frontend

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_shop.*
import android.widget.Button
import android.content.Intent

class Shop : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop)

        val backButton = findViewById(R.id.backButton) as Button
        backButton.setOnClickListener{
            val intent = Intent(this, PlayerScreen::class.java)
            startActivity(intent)
        }
    }
}
