package com.example.frontend

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import java.math.BigInteger
import java.security.MessageDigest
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URLEncoder
import java.net.URL
import java.nio.charset.StandardCharsets
import com.example.frontend.user
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*
import kotlin.collections.HashMap

const val EXTRA_MESSAGE = "com.example.frontEnd.Message"
val u = "USER"
val p = "PASS"


class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun sendPostRequest(loginData: JSONObject) {

        val queue = Volley.newRequestQueue(this)
        val url = "http://wrightg42.pythonanywhere.com/account/login"

        // Request a string response from the provided URL.
        val request = JsonObjectRequest(Request.Method.POST, url, loginData,
            Response.Listener { response ->
                //processing the json
                try {
                    println("Response: $response")

//                    val intent = Intent(this, PlayerScreen::class.java).apply {
//                        putExtra("POINTS", "$response['points']")
//                        putExtra("GROUPS", "$response['groups']")
//                    }
                    startActivity(intent)

                } catch (e: Exception) {
                    println("Exception: $e")
                }
            }, Response.ErrorListener {
                println("Volley error: $it")
                val t = Toast.makeText(applicationContext, "Invalid login", Toast.LENGTH_LONG)
                t.show()
            })

        queue.add(request)



    }

    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }

    fun checkDetails(user: String, pass: String): Boolean {
        return (user == u && pass.md5() == p.md5())
        println("test")
    }

    fun backToMain(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        // start your next activity
        startActivity(intent)
    }

    fun AttemptLogin(view: View) {
        val username = findViewById<EditText>(R.id.editUsername)
        val password = findViewById<EditText>(R.id.editPassword)
        if (username.text.toString() == "") {
            val t = Toast.makeText(applicationContext, "Please enter a username", Toast.LENGTH_LONG)
            t.show()

        } else if (password.text.toString() == "") {
            val t = Toast.makeText(applicationContext, "Please enter a password", Toast.LENGTH_LONG)
            t.show()
        } else {
            val jsonLogin = JSONObject()

            //val intent = Intent(this, PlayerScreen::class.java).apply {
            //    putExtra(EXTRA_MESSAGE,message)
            //}
            //  val loginUser = user()


            jsonLogin.put("email", username.text.toString())
            jsonLogin.put("pass_hash", password.text.toString())
            sendPostRequest(jsonLogin)
            startActivity(intent)
        }
    }
}