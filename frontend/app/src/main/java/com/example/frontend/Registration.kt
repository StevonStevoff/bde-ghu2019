package com.example.frontend

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.math.BigInteger
import java.security.MessageDigest

class Registration : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }

    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }

    fun backToMain(view: View){
        val intent = Intent(this, MainActivity::class.java)
        // start your next activity
        startActivity(intent)
    }


    fun sendPostRequest(loginData: JSONObject) : user {

        val queue = Volley.newRequestQueue(this)
        val url = "http://wrightg42.pythonanywhere.com/account/registration"
        val jO = JSONObject()

        // Request a string response from the provided URL.
        val request = JsonObjectRequest(Request.Method.POST, url, loginData,
            Response.Listener { response ->
                //processing the json
                try {
                    jO = response
                } catch (e: Exception) {
                    textView.text = "Exception: $e"
                }
            }, Response.ErrorListener {
                println("Volley error: $it")
            })

        queue.add(request)

        return user(jO.getString("email"), jO.getString("surname"), jO.getString("forename"), jO.getString("postcode"), jO.getInt("points"), jO.getDouble("distance"), jO.getInt("level") )
    }

        fun submitRegistration(view: View){
        val email = findViewById<EditText>(R.id.editEmail)
        val forename = findViewById<EditText>(R.id.editForename)
        val surname = findViewById<EditText>(R.id.editSurname)
        val password = findViewById<EditText>(R.id.editPassword)
        val confirmPassword = findViewById<EditText>(R.id.editConfirmPassword)
        val postcode = findViewById<EditText>(R.id.editPostcode)

        if (email.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please enter an email", Toast.LENGTH_LONG)
            t.show()

        }
        else if (forename.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please enter a forename", Toast.LENGTH_LONG)
            t.show()
        }
        else if (surname.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please enter a surname", Toast.LENGTH_LONG)
            t.show()
        }
        else if (password.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please enter a password", Toast.LENGTH_LONG)
            t.show()
        }
        else if (confirmPassword.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please confirm your password", Toast.LENGTH_LONG)
            t.show()
        }
        else if (postcode.text.toString() == "")
        {
            val t = Toast.makeText(applicationContext, "Please enter a postcode", Toast.LENGTH_LONG)
            t.show()
        }
        else if((password.text.toString() != confirmPassword.text.toString()))
        {
            val t = Toast.makeText(applicationContext, "Please enter matching passwords", Toast.LENGTH_LONG)
            t.show()
        }
        else
        {
            val jsonRegister = JSONObject()
            jsonRegister.put("email",email.text.toString())
            jsonRegister.put("forename",forename.text.toString())
            jsonRegister.put("surname",surname.text.toString())
            jsonRegister.put("password",password.text.toString().md5())
            jsonRegister.put("postcode",postcode.text.toString())

            val t = Toast.makeText(applicationContext, "Registration successful", Toast.LENGTH_LONG)
            t.show()
            val intent = Intent(this, MainActivity::class.java)
            val u: user = sendPostRequest(jsonRegister)
            startActivity(intent)
        }
    }
}
