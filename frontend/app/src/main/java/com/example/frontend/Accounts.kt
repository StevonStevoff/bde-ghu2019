package com.example.frontend

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Accounts : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts)
        val returnButton = findViewById(R.id.returnButton) as Button
        returnButton.setOnClickListener{
            val intent = Intent(this, PlayerScreen::class.java)
            startActivity(intent)
        }
    }
}
