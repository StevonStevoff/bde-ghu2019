package com.example.frontend

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_player_screen.*
import android.widget.Button
import android.content.Intent
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_shop.*
import kotlin.properties.Delegates

class Player(val lvl:Int,var fGs:Array<FollowerGroup>,val upgs:Array<String>){
    var level : Int = lvl
    var followerGroups : Array<FollowerGroup> = fGs
    var upgrades : Array<String> = upgs
}

class FollowerGroup(val c:String,val gS:Int,val ml:Double,val gL:Int){
    val city : String = c
    var groupSize : Int = gS
    val multiplier : Double = ml
    var groupLevel : Int = gL
}

class PlayerScreen : AppCompatActivity(), SensorEventListener {

    var running = false
    var sensorManager:SensorManager? = null
    var points = 0
    var groupOne = FollowerGroup("Manchester", 5,1.3,3)
    var groupTwo = FollowerGroup( "Belfast",20,1.5,2)
    var groupArray : Array<FollowerGroup> = arrayOf(groupOne,groupTwo)
    var upgradeArray : Array<String> = arrayOf("Boots of Might","Dildo of Truth")
    var sam = Player(1,groupArray,upgradeArray)


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_screen)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        followerDisplay.setText(sam.followerGroups.size)
        itemDisplay.setText(sam.upgrades.size)

        val accountsButton = findViewById(R.id.accountsButton) as ImageButton
        val shopButton = findViewById(R.id.shopButton) as ImageButton
        accountsButton.setOnClickListener{
            val intent = Intent(this, Accounts::class.java)
            startActivity(intent)
        }
        shopButton.setOnClickListener{
            val intent = Intent(this, Shop::class.java)
            startActivity(intent)
        }
    }


    override fun onResume(){
        super.onResume()
        running = true
        var stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)

        if(stepsSensor == null){
            Toast.makeText(this, "No Step Counter Sensor!", Toast.LENGTH_SHORT).show()
        }else{
            sensorManager?.registerListener(this, stepsSensor,SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onPause(){
        super.onPause()
        running = false
        sensorManager?.unregisterListener(this)
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int){}

    //this is the main function responsible for counting of steps
    override fun onSensorChanged(event: SensorEvent){
        if(running){
            var scorePerStep = 1//starts as one, counting the player
            //stepsValue is the on screen display of steps
            stepsValue.setText("" + event.values[0])
            //player's step ( always worth 1 )
            points += 1
            //calculating the score the player's group provides

            for(group in sam.followerGroups){
                //the number of people in the group * their multipliers
                var value = (group.groupSize * group.multiplier).toInt()
                scorePerStep += value
                points += value
            }
            //after loop is completed, display score per step
            scoreValue.setText("" + scorePerStep)

        }
    }

}
