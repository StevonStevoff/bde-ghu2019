package com.example.frontend

import com.example.frontend.Follower

class user{
//    email
//    surname
//    forename
//    password
//    postcode
    // level
    //follower groups
    // upgrades

    val Email: String
    val Surname: String
    val Forename: String
    val Postcode: String
    val Points: Int
    val Distance: Float
//    val FollowerGroups: Array<Follower>
    val Level: Int
//    val Upgrades: Array<String>

    constructor(emailAddress: String, surname: String, forename: String, postcode: String, points: Int, distance: Double, level: Int) {
        this.Email = emailAddress
        this.Surname = surname
        this.Forename = forename
        this.Postcode = postcode
        this.Points = points
        this.Distance = distance
//        this.FollowerGroups = followerGroup
        this.Level = level
//        this.Upgrades = upgrades

    }


}